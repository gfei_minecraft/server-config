#!/usr/bin/env python3

import requests
import json
import re
import sys

MOD_FILE = sys.argv[-1]
FORGE_URL = sys.argv[-2]

def flatten(list_of_lists):
    if len(list_of_lists) == 0:
        return list_of_lists
    if isinstance(list_of_lists[0], list):
        return flatten(list_of_lists[0]) + flatten(list_of_lists[1:])
    return list_of_lists[:1] + flatten(list_of_lists[1:])


regex = re.compile(r'.*[a-zA-Z].*')

version_dict = {}

mods = []
with open(MOD_FILE) as mod_list:
    for mod in mod_list:
        mods.append([x.strip() for x in mod.split('#')])

mod_count = len(mods)

for mod in mods:
    url = requests.get(FORGE_URL + '/' + mod[-1])
    data_json = json.loads(url.text)
    files = data_json['files']

    versions = set(flatten([x['versions'] for x in files]))
    versions = [i for i in versions if not regex.match(i)]
    versions.sort(key=lambda s: list(map(int, s.split('.'))))

    for version in versions:
        if version in version_dict:
            version_dict[version].append(data_json['title'])
        else:
            version_dict[version] = [data_json['title']]

all_versions = list(version_dict.keys())
all_versions.sort(key=lambda s: list(map(int, s.split('.'))))
max_version = all_versions[-1]

res = {k: v for k, v in version_dict.items() if len(v) == mod_count}

compatible_versions = list(res.keys())

if len(compatible_versions) > 1:
    compatible_versions.sort(key=lambda s: list(map(int, s.split('.'))))

final_version = compatible_versions[-1]

print(final_version)

if final_version != max_version:
    compatible = version_dict[final_version]
    latest = version_dict[max_version]
    mods_behind = [x for x in compatible if x not in latest]

    print("", file=sys.stderr)
    print("Best common minecraft version is", final_version, file=sys.stderr)
    print("Highest available minecraft version is", max_version, file=sys.stderr)
    print("", file=sys.stderr)
    print("Mods behind:\n ", "\n  ".join(mods_behind), file=sys.stderr)
    print("", file=sys.stderr)
    print("Mods ready:\n ", "\n  ".join(latest), file=sys.stderr)

