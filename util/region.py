#! /usr/bin/env python3
"""Utility for removing minecraft region files."""

import argparse
import math
import sys
import os
from os.path import expanduser

USERHOME = expanduser("~")

WORLD_PRESETS = {
    'overworld': os.path.join('world', 'region'),
    'nether': os.path.join('world', 'DIM-1', 'region'),
    'end': os.path.join('world', 'DIM1', 'region'),
    os.path.join('CUSTOM', 'PATH'): ''  # Dummy for help text
}


# -----------------------------------------------------------------------
# Custom argparse stuff.
# -----------------------------------------------------------------------
class MyParser(argparse.ArgumentParser):
    """Overide the default error behaviour of ArgumentParser.

    Print full usage text when an input error occurs.
    """

    def error(self, message):
        """Print error followed by usage text."""
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)


class CustomHelpFormatter(argparse.RawTextHelpFormatter):
    """Custom help formatter.

    Gets rid of duplicate text when there are both long and short options.
    """

    def __init__(self, prog):
        super().__init__(prog, max_help_position=30)

    def _format_action_invocation(self, action):
        if not action.option_strings or action.nargs == 0:
            return super()._format_action_invocation(action)
        default = self._get_default_metavar_for_optional(action)
        args_string = self._format_args(action, default)
        return ', '.join(action.option_strings) + ' ' + args_string

    def _get_help_string(self, action):
        help = action.help
        if '%(default)' not in action.help and action.dest != 'help':
            if action.default not in ['==SUPPRESS==', None, False]:
                defaulting_nargs = ['?', '*']
                if action.option_strings or action.nargs in defaulting_nargs:
                    help += ' (default: %(default)s)'

        return help


def bbox(astring):
    """Check for proper bounding box argument formatting."""
    box = [int(x) for x in astring.split(',')]

    if len(box) != 4:
        msg = 'Bounding box needs 4 values in the form "X1,Z1,X2,Z2". Only {} were given.'
        raise argparse.ArgumentTypeError(msg.format(len(box)))
    return astring


def bradius(astring):
    """Check for proper bounding radius argument formatting."""
    rad = [int(x) for x in astring.split(',')]
    if len(rad) != 3:
        msg = 'Bounding radius needs 3 values in the form "X,Z,Radius". Only {} were given.'
        raise argparse.ArgumentTypeError(msg.format(len(rad)))
    return astring


def setup_args():
    """Generate arguments."""
    # Add the custom help formatter.

    parser = argparse.ArgumentParser(
        formatter_class=lambda prog: CustomHelpFormatter(prog),
        description=('Delete region files that intersect a list of coordinate '
                     'regions.\n'
                     'Multiple boxes/radiuses may be given by repeating the '
                     '--box/--radius arguments.\nAt least one of either --box '
                     'or --radius is required for the script to work.\n\n'
                     '!!!! DANGER !!!!\n\n'
                     'This script can PERMANENTLY delete world files. '
                     'Make sure you know what you are doing.')
    )

    choices = WORLD_PRESETS.keys()

    parser.add_argument(
        '-b', '--box',
        type=bbox,
        help='box from opposite corners, formatted: "X1,Z1,X2,Z2"',
        action='append'
    )

    parser.add_argument(
        '-r', '--radius',
        type=bradius,
        help='radius around a center point, formatted: "X,Z,Radius"',
        action='append'
    )

    parser.add_argument(
        '-w', '--world',
        metavar='{{{}}}'.format(', '.join(choices)),
        help='world folder to act on.',
        default='overworld'
    )

    parser.add_argument(
        '-p', '--mcpath',
        type=str,
        default=os.path.join(USERHOME, 'server'),
        help='directory path of the server'
    )

    parser.add_argument(
        '-o', '--outer',
        help='keep files outside the specified area and delete marked areas',
        action='store_true'
    )

    parser.add_argument(
        '-t', '--test',
        help='do a test run without actually deleting any files',
        action='store_true'
    )

    parser.add_argument(
        '-f', '--format',
        help='specify a different template for region file names',
        default='r.{}.{}.mca'
    )

    # Generate args.
    args = parser.parse_args()

    if not (args.box or args.radius):
        parser.error('at least of one of --box or --radius is required')

    # Convert world key into path if possible.
    try:
        args.world = WORLD_PRESETS[args.world]
    except KeyError:
        # Assume custom path and pass through.
        pass

    return args


def region_file(x, z, name_format):
    """Generate region filename based on x,z coordinates.

    Parameters
    ----------
    x : int
        X coordinate contained within region file.
    z : int
        Z coordinate contained within region file.
    name_format : str, optional
        String for formatting the region file.

    Returns
    -------
    output
        a string representing the region file that contains the given
        coordinates.
    """
    region_size = 512

    output = name_format.format(
        int(math.floor(x / region_size)),
        int(math.floor(z / region_size))
    )

    return output


def keep_box(coord_list, name_format):
    """Get list of region files from list of boxes.

    Parameters
    ----------
    coord_list : list
        A list of coordinates defining boxes: [[X1,Z1,X2,Z2], [[X3,Z3,X4,Z4]]].
    name_format : str, optional
        String for formatting the region file.

    Returns
    -------
    regions
        a list of regions files that contain any blocks within the given boxes.
    """
    regions = []

    # Get region file for every x,z combo in each defined region.
    for box in coord_list:
        x_min = min([box[0][0], box[1][0]])
        x_max = max([box[0][0], box[1][0]]) + 1

        z_min = min([box[0][1], box[1][1]])
        z_max = max([box[0][1], box[1][1]]) + 1

        x_list = list(range(x_min, x_max))
        x_list.append(x_max)
        z_list = list(range(z_min, z_max))
        z_list.append(z_max)

        for x in x_list:
            for z in z_list:
                rgn = region_file(x, z, name_format)
                if rgn not in regions:
                    regions.append(rgn)

    return regions


def keep_radius(rad_list, name_format):
    """Get list of region files from list of circles: [[X1,Z1,R1], [[X2,Z2,R2]]].

    Parameters
    ----------
    rad_list : list
        A list of coordinates defining cirles.
    name_format : str, optional
        String for formatting the region file.

    Returns
    -------
    regions
        a list of regions files that contain any blocks within the given boxes.
    """
    regions = []

    for rad in rad_list:
        center = rad[:2]
        radius = rad[-1]
        print("Center: ", center, " with radius: ", radius)

        x_max = center[0] + radius
        x_min = center[0] - radius

        z_max = center[1] + radius
        z_min = center[1] - radius

        x_list = list(range(x_min, x_max))
        x_list.append(x_max)
        z_list = list(range(z_min, z_max))
        z_list.append(z_max)

        for x in x_list:
            for z in z_list:
                d = distance(x, z, center[0], center[1])
                if d <= radius:
                    rgn = region_file(x, z, name_format)
                    if rgn not in regions:
                        regions.append(rgn)

    return regions


def remove_regions(regions, region_path, outside, test_run):
    """Delete region files using list of files.

    Parameters
    ----------
    regions : list
        List of region files.
    outside : bool
        If true, delete the listed region files. Otherwise keep the listed
        files and delete the rest.
    test_run: bool
        If true, print the names of the files to be deleted but do not perform
        any action on the file system.
    """
    deleted = 0

    if not outside:
        print('Delete files not within bounding area.')
        # Move the saved files back to the world directory.
        for f in os.listdir(region_path):
            regionfile = os.path.join(region_path, f)
            if regionfile not in regions:
                if not test_run:
                    print("Delete [", f, "]")
                    os.remove(regionfile)
                    deleted += 1
                else:
                    print('Will delete: {}'.format(regionfile))

    else:
        print('Delete files within bounding area.')
        for region in regions:

            if not test_run:
                try:
                    print("Delete [", region, "]")
                    os.remove(region)
                    deleted += 1
                except FileNotFoundError:
                    print("Delete region file failed.")
                    continue
            else:
                print('Will delete: {}'.format(region))

    print('Deleted', deleted, 'region files.')


def distance(x1, z1, x2, z2):
    """Return the distance between two points."""
    return math.sqrt((x2-x1)**2 + (z2-z1)**2)


def main():
    args = setup_args()

    mcpath = args.mcpath
    region_folder = args.world
    outer = args.outer
    name_format = args.format
    test_run = args.test

    region_path = os.path.join(mcpath, region_folder)

    if not os.path.isdir(region_path) and not test_run:
        print('Region folder [{}] does not exits. Stopping.'.format(region_path))
        sys.exit(1)

    if args.radius:
        print('Using a bounding radius.')

        circles = []

        for rad in args.radius:
            rad = [int(x) for x in rad.split(',')]

            circles.append(rad)

        region_files = keep_radius(circles, name_format)
        regions = [os.path.join(region_path, x) for x in region_files]
        remove_regions(regions, region_path, outer, test_run)

    if args.box:
        print('Using bounding box.')
        coordinates = []

        for box in args.box:
            box = [int(x) for x in box.split(',')]

            coord = [box[:2], box[2:]]
            coordinates.append(coord)

        region_files = keep_box(coordinates, name_format)
        regions = [os.path.join(region_path, x) for x in region_files]
        remove_regions(regions, region_path, outer, test_run)


# -----------------------------------------------------------------------
# Run the script.
# -----------------------------------------------------------------------
if __name__ == '__main__':
    main()
