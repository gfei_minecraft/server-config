# Minecraft Server

A Minecraft server based on Fabric using Lithium for performance improvement.

## Commands
### start
Start the server.

### stop
Stop the server.

### restart 
Restart the server.

### status
Get the current status of server jar.  
If running will give the number of logged on players.

### backup
Incremental backup of server to configured folder.  
Runs the backup utility from the [dotfiles](https://gitlab.com/cgiacofei/dotfiles/-/raw/master/bin/rsync_backup) repo, and provides many options for creating backups.

### restore
Restore from incremental backup.  
Select a specific incremental backup to copy into the server directory.

### idle
Run an idle check. Server will shutdown if idle.

### update
Update server jar to latest release.

### reset_region
    usage: region.py [-h] [-b BOX] [-r RADIUS]
                    [-w {overworld, nether, end, CUSTOM/PATH}] [-p MCPATH] [-o]
                    [-t] [-f FORMAT]

    Delete region files that intersect a list of coordinate regions.
    Multiple boxes/radiuses may be given by repeating the --box/--radius arguments.
    At least one of either --box or --radius is required for the script to work.

    !!!! DANGER !!!!

    This script can PERMANENTLY delete world files. Make sure you know what you are doing.

    optional arguments:
    -h, --help                  show this help message and exit
    -b, --box BOX               box from opposite corners, formatted: "X1,Z1,X2,Z2"
    -r, --radius RADIUS         radius around a center point, formatted: "X,Z,Radius"
    -w, --world {overworld, nether, end, CUSTOM/PATH}
                                world folder to act on. (default: overworld)
    -p, --mcpath MCPATH         directory path of the server (default: /home/minecraft/server)
    -o, --outer                 keep files outside the specified area and delete marked areas
    -t, --test                  do a test run without actually deleting any files
    -f, --format FORMAT         specify a different template for region file names (default: r.{}.{}.mca)
